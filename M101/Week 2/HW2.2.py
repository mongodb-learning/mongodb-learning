import pymongo

# establish a connection to the database
connection = pymongo.Connection("mongodb://localhost", safe=True)

# get a handle to the students database
db=connection.students
grades = db.grades

def removeLowestHomework():
    cur = grades.find({"type": "homework"}).sort([('student_id', pymongo.ASCENDING), ('score', pymongo.ASCENDING)])
    
    current_student_id = None
    for item in cur:
        if (current_student_id is None or current_student_id != item['student_id']):
            current_student_id = item['student_id']
            grades.remove({'_id': item['_id']})
        
    
    
removeLowestHomework();
