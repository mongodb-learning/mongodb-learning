import pymongo

from pymongo import MongoClient

# connect to database
connection = MongoClient('localhost', 27017)

db = connection.school
students = db.students

all_students = students.find()
for student in all_students:
    
    temp = None
    for item in student['scores']:
        if (item['type'] == "homework" and (temp == None or temp['score'] > item['score'])):
            temp = item
            
    if temp != None:
        # First way:
        #student['scores'].remove(temp)
        #students.update({'_id': student['_id']}, {'$set': {'scores': student['scores']}})
        # Second way:
        students.update({'_id': student['_id']}, {'$pull': {'scores': temp}})
        
        
#ANSWER IS 13

